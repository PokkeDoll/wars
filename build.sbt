name := "Wars"

version := "2.1.4-Final"

scalaVersion := "2.13.3"

//resolvers += "papermc" at "https://papermc.io/repo/repository/maven-public/"
//resolvers += "spigot-repo" at "https://hub.spigotmc.org/nexus/content/repositories/snapshots/"
resolvers += "jitpack.io" at "https://jitpack.io"

libraryDependencies ++= Seq(
  //"com.destroystokyo.paper" % "paper-api" % "1.12.2-R0.1-SNAPSHOT",
  //"org.spigotmc" % "spigot-api" % "1.12.2-R0.1-SNAPSHOT",
  "com.zaxxer" % "HikariCP" % "3.4.2",
  //"com.github.MilkBowl" % "VaultAPI" % "1.7"
)

val libs = Seq(
  "lib/scala-library-2.13.1.jar",
  "lib/HikariCP-3.4.2.jar",
  "lib/Takuya-1.0.jar",
  "lib/VotePoint-1.0.jar"
)

packageOptions in (Compile, packageBin) +=
  Package.ManifestAttributes("Class-Path" -> libs.mkString(" "))

artifactName :={(sv: ScalaVersion,module: ModuleID, artifact: Artifact) => "Wars-" + module.revision + "." + artifact.extension}
