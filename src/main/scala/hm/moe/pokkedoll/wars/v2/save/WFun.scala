package hm.moe.pokkedoll.wars.v2.save

trait WFun {
  var tag: String
  var tagContainer: Vector[String]

  var sound: String
  var soundContainer: Vector[String]

  var effect: String
  var effectContainer: Vector[String]
}
