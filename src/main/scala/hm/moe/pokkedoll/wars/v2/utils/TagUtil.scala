package hm.moe.pokkedoll.wars.v2.utils

import hm.moe.pokkedoll.wars.v2.save.WPlayer
import hm.moe.pokkedoll.wars.v2.{Wars, WarsAPI}
import org.bukkit.ChatColor
import org.bukkit.configuration.ConfigurationSection
import org.bukkit.entity.Player

import scala.collection.mutable

class TagUtil(plugin: Wars, cs: ConfigurationSection) {
  val tags = mutable.HashMap.empty[String, String]

  def reload(): Unit = {
    tags.clear()
    cs.getKeys(false).forEach(f => {tags.put(f, cs.getString(f))})
  }

  reload()

  def setTag(player: Player, tag: String): Unit = {
    setTag(WarsAPI.getWPlayer(player), tag)
  }

  def setTag(wp: WPlayer, tag: String): Unit = {
    if(wp.tag!=tag) {
      wp.tag = tag
      updateTag(wp.player, tag)
    }
  }

  def updateTag(player: Player, tag: String): Unit = {
    tags.get(tag) match {
      case Some(t) =>
        player.setDisplayName(ChatColor.translateAlternateColorCodes('&', s"&7[$t&7] &f${player.getName}"))
      case None =>
        player.setDisplayName(player.getName)
    }
  }

  def updateTag(player: Player): Unit = {
    val wp = WarsAPI.getWPlayer(player)
    updateTag(player, wp.tag)
  }

  /**
   * 事前に手続きを済ませるべし
   */
  def buyTag(wp: WPlayer, tag: String): Unit = {
    wp.tagContainer +:= tag
    plugin.getDatabase.updateWPlayer(wp.player)
  }
}
