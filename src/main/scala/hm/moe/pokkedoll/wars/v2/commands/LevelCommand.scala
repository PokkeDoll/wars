package hm.moe.pokkedoll.wars.v2.commands

import hm.moe.pokkedoll.wars.v2.save.WPlayer
import hm.moe.pokkedoll.wars.v2.{Wars, WarsAPI}
import org.bukkit.Bukkit
import org.bukkit.command.{Command, CommandExecutor, CommandSender}
import org.bukkit.entity.Player

class LevelCommand(plugin: Wars) extends CommandExecutor {
  override def onCommand(sender: CommandSender, command: Command, label: String, args: Array[String]): Boolean = {
    sender match {
      case player: Player =>
        if(args.length==0) {
          show(player, WarsAPI.getWPlayer(player))
        } else {
          val target = Bukkit.getPlayer(args(0))
          if(target == null) {
            player.sendMessage(s"§c${target.getName}はオフラインです!")
          } else {
            show(player, WarsAPI.getWPlayer(target))
          }
        }
      case _ =>
    }
    true
  }
  private def show(player: Player, wp: WPlayer): Unit = {
    player.sendMessage(
      s"§a現在のレベル: §e${wp.level}\n" +
        s"§a現在の経験値: §e${wp.exp}§7/§e${plugin.getLevelUtil.getExp(wp.level)}"
    )
  }
}
