package hm.moe.pokkedoll.wars.v2.events

import org.bukkit.entity.Player
import org.bukkit.event.{Event, HandlerList}

class WeaponUpgradeEvent(val player: Player, val from: String, val to: String, val success: Boolean) extends Event {
  override def getHandlers: HandlerList = WeaponUpgradeEvent.handlers
}

object WeaponUpgradeEvent {
  val handlers = new HandlerList()
  def getHandlerList: HandlerList = handlers
}
