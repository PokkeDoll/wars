package hm.moe.pokkedoll.wars.v2

import org.bukkit.{Bukkit, Location}

import scala.util.Try

object LocationSerializer {
  private lazy val world = Bukkit.getWorld("war")

  def string2location(string: String): Option[Location] = {
    try {
      // ここでIndexOut...
      val arr = string.split(",")
      // ここでNumberFormat..., NullPointer...
      val loc = new Location(world, arr(0).toDouble, arr(1).toDouble, arr(2).toDouble, arr(3).toFloat, arr(4).toFloat)
      Some(loc)
    } catch {
      case e: Exception => None
    }
  }

  def location2string(location: Location): String = {
    s"${location.getX},${location.getY},${location.getZ},${location.getYaw},${location.getPitch}"
  }
}
