package hm.moe.pokkedoll.wars.v2.save

trait WStats {
  var exp: Int
  var level: Int
}
