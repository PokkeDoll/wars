package hm.moe.pokkedoll.wars.v2.db

import java.sql.SQLException
import java.util.UUID

import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import hm.moe.pokkedoll.wars.v2.{Wars, WarsAPI}
import hm.moe.pokkedoll.wars.v2.save.WPlayer
import org.bukkit.entity.Player

import scala.collection.mutable

class Database(plugin: Wars) {
  private val dbPath = "database.db"
  private val config = new HikariConfig()

  config.setDriverClassName("org.sqlite.JDBC")
  config.setJdbcUrl(s"jdbc:sqlite:${plugin.getDataFolder.getAbsolutePath}/$dbPath")
  config.setConnectionInitSql("SELECT 1")

  val hikari = new HikariDataSource(config)

  private val ACCOUNT = "account"

  // タグ、キルサウンド、キルエフェクト
  private val FUN = "fun"
  private val TDM = "tdm"

  def hasUUID(uuid: UUID): Boolean = {
    val c = hikari.getConnection
    val ps = c.prepareStatement(s"SELECT `uuid` FROM `$ACCOUNT` WHERE `uuid`=?")
    try {
      ps.setString(1, uuid.toString)
      val rs = ps.executeQuery()
      rs.next()
    } catch {
      case e: SQLException => e.printStackTrace(); false
    } finally {
      if(!ps.isClosed) ps.close()
      if(!c.isClosed) c.close()
    }
  }

  def getName(uuid: UUID): Option[String] = {
    val c = hikari.getConnection
    val ps = c.prepareStatement(s"SELECT `name` FROM `$ACCOUNT` WHERE `uuid`=?")
    try {
      ps.setString(1, uuid.toString)
      val rs = ps.executeQuery()
      if(rs.next()) {
        Some(rs.getString("name"))
      } else {
        None
      }
    } catch {
      case e: SQLException => e.printStackTrace(); None
    } finally {
      if(!ps.isClosed) ps.close()
      if(!c.isClosed) c.close()
    }
  }

  def getUUID(name: String): Option[UUID] = {
    val c = hikari.getConnection
    val ps = c.prepareStatement(s"SELECT `uuid` FROM `$ACCOUNT` WHERE `name`=?")
    try {
      ps.setString(1, name)
      val rs = ps.executeQuery()
      if(rs.next()) {
        Some(UUID.fromString(rs.getString("uuid")))
      } else {
        None
      }
    } catch {
      case e: SQLException => e.printStackTrace(); None
    } finally {
      if(!ps.isClosed) ps.close()
      if(!c.isClosed) c.close()
    }
  }

  def loadWPlayer(player: Player): WPlayer = {
    val c = hikari.getConnection
    val wp = new WPlayer(player)
    try {
      val ps = c.prepareStatement(s"SELECT * FROM `$ACCOUNT` WHERE `uuid`=?")
      ps.setString(1, player.getUniqueId.toString)
      val rs = ps.executeQuery()
      if(rs.next()) {
        wp.level = rs.getInt("level")
        wp.exp = rs.getInt("exp")
        rs.close()
        ps.close()
        val ps2 = c.prepareStatement(s"SELECT * FROM `$FUN` WHERE `uuid`=?")
        ps2.setString(1, player.getUniqueId.toString)
        val rs2 = ps2.executeQuery()
        if(rs2.next()) {
          wp.tag = rs2.getString("tag")
          val tagContainer = rs2.getString("tagContainer")
          wp.tagContainer = if(tagContainer=="") Vector.empty[String] else tagContainer.split("@tag@").toVector
          wp.sound = rs2.getString("sound")
          val soundContainer = rs2.getString("soundContainer")
          wp.soundContainer = if(soundContainer=="") Vector.empty[String] else soundContainer.split("@sound@").toVector
          wp.effect = rs2.getString("effect")
          val effectContainer = rs2.getString("effectContainer")
          wp.effectContainer = if(effectContainer=="") Vector.empty[String] else effectContainer.split("@effect").toVector
        }
      } else {
        ps.close()
        val ps2 = c.prepareStatement(s"INSERT INTO `$ACCOUNT`(`uuid`, `name`) VALUES(?, ?)")
        ps2.setString(1, player.getUniqueId.toString)
        ps2.setString(2, player.getName)
        ps2.executeUpdate()
        ps2.close()
        val ps3 = c.prepareStatement(s"INSERT INTO `$FUN`(`uuid`) VALUES(?)")
        ps3.setString(1, player.getUniqueId.toString)
        ps3.executeUpdate()
        ps3.close()
        wp.level = 1
        wp.exp = 0
        wp.tag = ""
        wp.tagContainer = Vector.empty[String]
        wp.sound = ""
        wp.soundContainer = Vector.empty[String]
        wp.effect = ""
        wp.effectContainer = Vector.empty[String]
      }
      wp
    } catch {
      case e: SQLException =>
        e.printStackTrace()
        wp
    } finally {
      c.close()
    }
  }

  def updateWPlayer(player: Player*): Boolean = {
    val c = hikari.getConnection
    val psAccount = c.prepareStatement(s"UPDATE `$ACCOUNT` SET `level`=?, `exp`=? WHERE `uuid`=?")
    val psFun = c.prepareStatement(s"UPDATE `$FUN` SET `tag`=?, `tagContainer`=?, `sound`=?, `soundContainer`=?, `effect`=?, `effectContainer`=? WHERE `uuid`=?")
    try {
      player.foreach(p => {
        val wp = WarsAPI.getWPlayer(p)
        psAccount.setInt(1, wp.level)
        psAccount.setInt(2, wp.exp)
        psAccount.setString(3, p.getUniqueId.toString)
        psFun.setString(1, wp.tag)
        psFun.setString(2, if(wp.tagContainer.isEmpty) "" else wp.tagContainer.mkString("@tag@"))
        psFun.setString(3, wp.sound)
        psFun.setString(4, if(wp.soundContainer.isEmpty) "" else wp.soundContainer.mkString("@sound@"))
        psFun.setString(5, wp.effect)
        psFun.setString(6, if(wp.effectContainer.isEmpty) "" else wp.effectContainer.mkString("@effect@"))
        psFun.setString(7, p.getUniqueId.toString)
        psAccount.executeUpdate()
        psFun.executeUpdate()
      })
      true
    } catch {
      case e: SQLException =>
        e.printStackTrace()
        false
    } finally {
      psFun.close()
      psAccount.close()
      c.close()
    }

  }
}
