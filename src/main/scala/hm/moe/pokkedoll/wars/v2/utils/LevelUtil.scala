package hm.moe.pokkedoll.wars.v2.utils

import hm.moe.pokkedoll.wars.v2.{Wars, WarsAPI}
import net.md_5.bungee.api.ChatColor
import net.md_5.bungee.api.chat.ComponentBuilder
import org.bukkit.{Bukkit, Sound}
import org.bukkit.entity.Player

class LevelUtil(plugin: Wars) {

  def getExp(level: Int): Int = (100*Math.pow(1.05, level+5)+200).toInt

  def addExp(player: Player, amount: Int): Unit = {
    val wp = WarsAPI.getWPlayer(player)
    if(wp.exp >= getExp(wp.level)) {
      val nextLevel = wp.level+1
      Bukkit.broadcast(s"§a${wp.player.getName}§dがレベル§${nextLevel}§dに上がりました！", "pokkedoll.0")
      player.sendMessage(s"次のレベルに上がるまであと§a${getExp(nextLevel)}exp§f必要です")
      player.playSound(player.getLocation, Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 1.0f)
      wp.level = nextLevel
      wp.exp = 0
      plugin.getDatabase.updateWPlayer(player)
    } else {
      wp.exp += amount
      player.sendMessage(s"§9${amount}exp獲得しました")
    }
  }

  def addExpSk(players: java.util.List[Player], amount: Int): Unit = {
    players.stream().forEach(addExp(_, amount))
  }
}
