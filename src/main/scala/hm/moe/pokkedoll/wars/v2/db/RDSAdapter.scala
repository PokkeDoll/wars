package hm.moe.pokkedoll.wars.v2.db

import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import hm.moe.pokkedoll.takuya.Takuya
import hm.moe.pokkedoll.vp.VotePoint
import hm.moe.pokkedoll.wars.v2.Wars

class RDSAdapter(plugin: Wars) {
  // 初期化
  private val hc = new HikariConfig()
  // MySQLドライバ
  hc.setDriverClassName("com.mysql.jdbc.Driver")
  // URL
  hc.setJdbcUrl(s"jdbc:mysql://pokkedoll.cwhnr4fjrjha.ap-northeast-1.rds.amazonaws.com:3306/pokkedoll")
  // ユーザー名/パスワード
  hc.addDataSourceProperty("user", "doll")
  hc.addDataSourceProperty("password", "F8av+f429vjduJsorMfale")
  // キャッシュ系の設定
  hc.addDataSourceProperty("cachePrepStmts", "true")
  hc.addDataSourceProperty("prepStmtCacheSize", "250")
  hc.addDataSourceProperty("prepStmtCacheSqlLimit", "2048")
  // サーバー再度プリペアードステートメントの使用
  hc.addDataSourceProperty("useServerPrepStmts", "true")
  // SSL
  hc.addDataSourceProperty("useSSL", "false")
  //接続をテストするためのクエリ
  hc.setConnectionInitSql("SELECT 1")

  val hikari = new HikariDataSource(hc)

  val votePoint = new VotePoint(hikari)
  val takuya = new Takuya(plugin , hikari)
}
