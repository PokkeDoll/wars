package hm.moe.pokkedoll.wars.v2.commands

import java.util
import java.util.Collections

import hm.moe.pokkedoll.wars.v2.{Wars, WarsAPI}
import org.bukkit.Material
import org.bukkit.command.{Command, CommandSender, TabExecutor}
import org.bukkit.entity.Player

class ItemCommand(wars: Wars) extends TabExecutor {
  override def onCommand(sender: CommandSender, command: Command, label: String, args: Array[String]): Boolean = {
    sender match {
      case player: Player =>
        val v0 = if(args.length==0) "" else args(0)
        if(v0.equalsIgnoreCase("list")) {
          val s = new StringBuilder("アイテム一覧\n")
          WarsAPI.items.keys.foreach(key => s.append(s"$key\n"))
          sender.sendMessage(s.toString())
        } else if (args.length > 1 && v0.equalsIgnoreCase("set")) {
          val item = player.getInventory.getItemInMainHand
          if(item == null || item.getType == Material.AIR) {
            player.sendMessage("無効なアイテムです")
          } else {
            WarsAPI.setItem(args(1), item.clone())
            player.sendMessage(s"${args(1)}をセットしました")
          }
        } else if (args.length > 1 && v0.equalsIgnoreCase("remove")) {
          WarsAPI.removeItem(args(1))
          player.sendMessage(s"${args(1)}を削除しました")
        }  else if (args.length > 1 && v0.equalsIgnoreCase("get")) {
            WarsAPI.items.get(args(1)) match {
              case Some(item) =>
                player.sendMessage(s"${args(1)}を入手しました")
                player.getInventory.addItem(item)
              case None =>
                player.sendMessage(s"${args(1)}は存在しません")
            }
        } else if(v0.equalsIgnoreCase("save")) {
          WarsAPI.saveItem()
          player.sendMessage("セーブしました")
        } else if(v0.equalsIgnoreCase("reload")) {
          WarsAPI.reloadItem()
          player.sendMessage("リロードしました")
        } else {
          sender.sendMessage(
            "構文: /item (list|set|remove|save|reload) (...)\n" +
              "/item list: 登録されているアイテムのリストを表示\n" +
              "/item set A: 手に持ってるアイテムをAとして登録\n" +
              "/item remove A: Aを削除\n" +
              "/item get A: Aを入手\n" +
              "/item save: コンフィグをセーブ\n" +
              "/item reload: コンフィグをリロード, セーブ前のデータは消滅する"
          )
        }
    }
    true
  }

  override def onTabComplete(sender: CommandSender, command: Command, alias: String, args: Array[String]): util.List[String] = {
    if(args.length == 1) {
      if(args(0).length == 0) {
        util.Arrays.asList("list", "set", "remove", "get", "save", "reload")
      } else {
        val v0 = args(0)
        if("".startsWith(v0)) {
          return Collections.singletonList("")
        }
      }
    }
    null
  }
}
