package hm.moe.pokkedoll.wars.v2.listeners

import java.util

import com.connorlinfoot.actionbarapi.ActionBarAPI
import hm.moe.pokkedoll.wars.v2.utils.ShopUtil.ShopItem
import hm.moe.pokkedoll.wars.v2.{Wars, WarsAPI}
import me.DeeCaaD.CrackShotPlus.Events.WeaponPickupEvent
import org.bukkit.block.Sign
import org.bukkit.entity.{EntityType, Player}
import org.bukkit.event.entity.PlayerDeathEvent
import org.bukkit.event.inventory._
import org.bukkit.event.player._
import org.bukkit.event.{EventHandler, Listener}
import org.bukkit.inventory.{EquipmentSlot, Inventory, ItemStack}
import org.bukkit.scheduler.BukkitRunnable
import org.bukkit.{Bukkit, ChatColor, GameMode, Material, OfflinePlayer, Sound}

class PlayerListener(plugin: Wars) extends Listener {

  private lazy val su = plugin.getShopUtil

  private lazy val uu = plugin.getUpgradeUtil

  private lazy val takuya = plugin.getRDSAdapter.takuya

  @EventHandler
  def onWeaponPickup(e: WeaponPickupEvent): Unit = {
    if(!e.getPlayer.isSneaking) {
      e.setCancelled(true)
      ActionBarAPI.sendActionBar(e.getPlayer, "武器を入手するにはスニークしてください！")
    }
  }

  @EventHandler
  def onClick(e: InventoryClickEvent): Unit = {
    val player = e.getWhoClicked
    if(e.getClickedInventory != null) {
      val inv = e.getClickedInventory
      val title = e.getView.getTitle
      su.shops.get(title).foreach(i => {
        e.setCancelled(true)
        if(e.getCurrentItem==null || e.getCurrentItem.getType==Material.AIR || e.getCurrentItem.getType==Material.STONE_SPADE) return
        if(inv.getType==InventoryType.PLAYER) return
        val shopi: ShopItem = i(e.getSlot-9)
        if(shopi.level > WarsAPI.getWPlayer(player.asInstanceOf[Player]).level) {
          player.sendMessage("§c1レベルが足りません!")
        } else if (!Wars.getEcon.has(player.asInstanceOf[OfflinePlayer], shopi.price)) {
          player.sendMessage("§c所持金が足りません！")
        } else {
          Wars.getEcon.withdrawPlayer(player.asInstanceOf[OfflinePlayer], shopi.price)
          player.sendMessage(
            s"${WarsAPI.getItemName(shopi.item)}§9 を購入しました\n" +
            s"§9${shopi.price} dollersを失いました"
          )
          player.closeInventory()
        }
      })
      if(inv.getType == InventoryType.ANVIL) {
        if(e.getSlot==2 && e.getClick==ClickType.LEFT) {
          val item = inv.getItem(0)
          val tool = inv.getItem(1)
          if(item == null || tool==null) {
            e.setCancelled(true)
            return
          }
          uu.getUpgradeItem(item) match {
            case Some(upgradeItem) =>
              val result = inv.getItem(2)
              if(result==null && result.getType==Material.AIR) {
                e.setCancelled(true)
                return
              }
              val chance: Double = if(result.getItemMeta.hasLore) result.getItemMeta.getLore.get(0).replaceAll("§f成功確率: §a", "").replaceAll("%", "").toDouble else 0.0
              if(WarsAPI.randomChance(chance)) {
                inv.setItem(0, new ItemStack(Material.AIR))
                inv.setItem(1, new ItemStack(Material.AIR))
                inv.setItem(2, new ItemStack(Material.AIR))
                val key = WarsAPI.getItemKey(tool)
                val success = if(upgradeItem.list.contains(key)) upgradeItem.list.get(key) else upgradeItem.list.get("else")
                WarsAPI.items.get(success.getOrElse(("",0.0))._1) match {
                  case Some(value) =>
                    player.setItemOnCursor(value)
                    player.sendMessage("§9成功しました!")
                    player.getWorld.playSound(player.getLocation, Sound.BLOCK_ANVIL_USE, 1.0f, 1.0f)
                    player.getWorld.playSound(player.getLocation, Sound.ENTITY_PLAYER_LEVELUP, 1.0f, 2.0f)
                    WarsAPI.spawnFirework(player.getLocation)
                }
              } else {
                player.sendMessage("§c失敗しました...")
                inv.setItem(1, new ItemStack(Material.AIR))
                inv.setItem(2, new ItemStack(Material.AIR))
                player.getWorld.playSound(player.getLocation, Sound.BLOCK_ANVIL_DESTROY, 1.0f, 1.0f)
              }
            case None =>
          }
        }
      }
    }
  }

  @EventHandler
  def onDeath(e: PlayerDeathEvent): Unit = {
    e.getEntity.spigot().respawn()
  }

  val NAME = "配送サービス・タクヤ"

  @EventHandler
  def onInteractEntity(e: PlayerInteractAtEntityEvent): Unit = {
    if(e.getRightClicked==null) return
    val entity = e.getRightClicked
    if(entity.getCustomName != null) {
      if(entity.getCustomName == NAME) {
        val player = e.getPlayer
        e.setCancelled(true)
        val inv: Inventory = Bukkit.createInventory(null, 54, s"${player.getName}'s Bridge Chest")
        takuya.getInventory(e.getPlayer.getUniqueId) match {
          case Some(items) =>
            inv.setContents(items)
          case None =>
            player.sendMessage("INFO: チェストは空です")
        }
        player.openInventory(inv)
      } else {
        su.createInventory(WarsAPI.getWPlayer(e.getPlayer), entity.getCustomName).foreach(i => {
          e.setCancelled(true)
          new BukkitRunnable {
            override def run(): Unit = {
              e.getPlayer.openInventory(i)
            }
          }.runTaskLater(plugin, 1L)
        })
      }
      val item = e.getPlayer.getInventory.getItemInMainHand
      if(item!=null && item.getType==Material.NAME_TAG && entity.getType==EntityType.ARMOR_STAND) {
        e.setCancelled(true)
        val currentName = entity.getCustomName
        new BukkitRunnable {
          override def run(): Unit = {
            entity.setCustomName(currentName)
          }
        }.runTaskLater(plugin, 1L)
      }
    }
  }

  @EventHandler
  def onInteract(e: PlayerInteractEvent): Unit = {
    val player = e.getPlayer
    if (e.getClickedBlock != null && e.getClickedBlock.getState.isInstanceOf[Sign]) {
      val sign = e.getClickedBlock.getState.asInstanceOf[Sign]
      if (sign.getLine(0) == ChatColor.translateAlternateColorCodes('&', "&9&l== VOTE POINT ==")) {
        plugin.getRDSAdapter.votePoint.tradeVP(player)
      }
    }
    if(e.getItem==null || e.getHand==EquipmentSlot.OFF_HAND) return
    val item = e.getItem
    if(item.getType==Material.NAME_TAG) {
      e.setCancelled(true)
      val name = WarsAPI.getItemName(item)
      if(name.contains("Tag: ")) {
        val tag = name.replaceAll("Tag: ", "")
        if(plugin.getTagUtil.tags.contains(tag)) {
          val wp = WarsAPI.getWPlayer(player)
          if(wp.tagContainer.contains(tag)) {
            player.sendMessage("§cすでにタグを所持しています")
          } else {
            plugin.getTagUtil.buyTag(wp, tag)
            player.getInventory.getItemInMainHand.setType(Material.AIR)
            player.updateInventory()
            player.sendMessage("§9新しいタグを獲得しました!")
          }
        }
      }
    }
  }

  @EventHandler
  def onDrop(e: PlayerDropItemEvent): Unit = {
    if(e.getPlayer.getGameMode != GameMode.CREATIVE && plugin.getCSUtility.getWeaponTitle(e.getItemDrop.getItemStack) == null) {
      e.setCancelled(true)
    }
  }

  @EventHandler
  def onAnvilPrepare(e: PrepareAnvilEvent): Unit = {
    // 元となるアイテム
    val item = e.getInventory.getItem(0)
    if(item==null) return
    if(uu.isUpgradeItem(item)) {
      // 強化素材となるアイテム
      val tool = e.getInventory.getItem(1)
      if(tool!=null) {
        // 素材が持つ成功確率
        val baseChance: Double = uu.getChance(tool)
        uu.getUpgradeItem(item) match {
          case Some(upgradeItem) =>
            val key = WarsAPI.getItemKey(tool)
            upgradeItem.list.get(if(upgradeItem.list.contains(key)) key else "else") match {
              case Some(value) =>
                val result = WarsAPI.items.getOrElse(value._1, uu.invalidItem).clone()
                val rMeta = result.getItemMeta
                val chance = if(baseChance - value._2 > 0) baseChance - value._2 else 0.0
                rMeta.setLore(util.Arrays.asList(s"§f成功確率: §a${chance*tool.getAmount}%", "§4§n確率で失敗します!!"))
                result.setItemMeta(rMeta)
                e.setResult(result)
                e.getInventory.setRepairCost(1)
                return
              case _ =>
            }
          case _ =>
        }
      }
    }
    e.getInventory.setRepairCost(40)
  }

  //TODO 短縮する
  @EventHandler
  def onJoin(e: PlayerJoinEvent): Unit = {
    WarsAPI.getWPlayer(e.getPlayer)
    plugin.getTagUtil.updateTag(e.getPlayer)
    if(!takuya.hasUUID(e.getPlayer.getUniqueId.toString)) {
      takuya.insert(e.getPlayer.getUniqueId.toString)
    }
  }

  @EventHandler
  def onQuit(e: PlayerQuitEvent): Unit = {
    plugin.getDatabase.updateWPlayer(e.getPlayer)
  }

  @EventHandler
  def onClose(e: InventoryCloseEvent): Unit = {
    if(e.getInventory.getTitle==s"${e.getPlayer.getName}'s Bridge Chest") {
      takuya.setInventory(e.getPlayer.getUniqueId, e.getInventory.getContents)
    }
  }
}
