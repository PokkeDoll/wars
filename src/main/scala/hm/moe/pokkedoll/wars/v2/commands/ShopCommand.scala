package hm.moe.pokkedoll.wars.v2.commands

import hm.moe.pokkedoll.wars.v2.Wars
import hm.moe.pokkedoll.wars.v2.utils.ShopUtil.ShopItem
import org.bukkit.command.{Command, CommandExecutor, CommandSender}

class ShopCommand(private val plugin: Wars) extends CommandExecutor {

  private lazy val shop = plugin.getShopUtil

  override def onCommand(sender: CommandSender, command: Command, label: String, args: Array[String]): Boolean = {
    if(args.length == 0) false
    else {
      if(args(0).equalsIgnoreCase("list")) {
        val sb = new StringBuilder("ショップIDリスト\n")
        shop.shops.keys.foreach(f => sb.append(f + "\n"))
        sender.sendMessage(sb.toString())
      } else if (args.length > 1 && args(0).equalsIgnoreCase("info")) {
        shop.shops.get(args(1)) match {
          case Some(value) =>
            val sb = new StringBuilder(s"${args(1)}の商品リスト\n")
            value.foreach(f => sb.append(s"ID: ${f.id}, Price: ${f.price}, Level: ${f.level}\n"))
            sender.sendMessage(sb.toString())
          case None =>
            sender.sendMessage(s"${args(1)}は存在しません")
        }
      } else if (args.length > 1 && args(0).equalsIgnoreCase("create")) {
        val v1 = args(1)
        shop.shops.get(v1) match {
          case Some(_) =>
            sender.sendMessage(s"${v1}はすでに存在します")
          case None =>
            shop.createShop(v1)
            sender.sendMessage(s"${v1}を作成しました")
        }
      } else if(args.length > 1 && args(0).equalsIgnoreCase("delete")) {
        val v1 = args(1)
        shop.shops.get(v1) match {
          case Some(_) =>
            shop.deleteShop(v1)
            sender.sendMessage(s"${v1}を削除しました")
          case None =>
            sender.sendMessage(s"${v1}は存在しません")
        }
      } else if(args(0).equalsIgnoreCase("mod")) {
        val v1 = args(1)
        shop.shops.get(v1) match {
          case Some(value) =>
            if(args.length > 5 && args(2).equalsIgnoreCase("set")) {
              shop.addShopItem(v1, args(3), args(4).toInt, args(5).toInt)
              sender.sendMessage(s"${args(3)}を${v1}にセットしました")
              /*
              val n = value :+ new ShopItem(args(3), args(4).toInt, args(5).toInt)
              shop.shops.put(v1, n)
              plugin.getConfig.set(s"shops.$v1", shop.toStringList(n))
              plugin.saveConfig()
              sender.sendMessage(s"${args(3)}を${v1}にセットしました")
               */
            } else if(args.length > 3 && args(2).equalsIgnoreCase("remove")) {
              shop.removeShopItem(v1, args(3))
              sender.sendMessage(s"${args(3)}を${v1}から削除しました")
              /*
              val r = value.filter(f => f.id == args(3))
              shop.shops.put(v1, r)
              plugin.getConfig.set(s"shops.$v1", shop.toStringList(r))
              plugin.saveConfig()
              sender.sendMessage(s"${args(3)}を${v1}から削除しました")
               */
            } else {
              sender.sendMessage("引数が間違っています. /shop helpで確認")
            }
          case None =>
            sender.sendMessage(s"${v1}は存在しません")
        }
      } else if(args(0) .equalsIgnoreCase("reload")) {
        plugin.reloadConfig()
        shop.reload()
        sender.sendMessage("リロードしました")
      } else {
        sender.sendMessage(
          "使い方: /shop\n" +
          "/shop list: ショップIDを取得する\n" +
          "/shop info <id>: §c@Unsafe§f ショップの詳細を取得する\n" +
          "/shop create <id>: §c@Unsafe§f ショップを作成する\n" +
          "/shop delete <id>: §c@Unsafe§f ショップを完全に削除する\n" +
          "/shop mod <id> set <itemID> <price> <rank>: §c@Unsafe§f <ItemID>を販売する\n" +
          "/shop mod <id> remove <itemID>: §c@Unsafe§f <ItemID>を削除する\n"
        )
      }
      true
    }
  }
}
