package hm.moe.pokkedoll.wars.v2

import java.util

import com.shampaggon.crackshot.CSUtility
import hm.moe.pokkedoll.nick.Nick
import hm.moe.pokkedoll.wars.v2.commands._
import hm.moe.pokkedoll.wars.v2.db.{Database, RDSAdapter}
import hm.moe.pokkedoll.wars.v2.listeners.PlayerListener
import hm.moe.pokkedoll.wars.v2.utils.{LevelUtil, ShopUtil, TagUtil, UpgradeUtil}
import net.milkbowl.vault.economy.Economy
import org.bukkit.{Bukkit, Material}
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin

class Wars extends JavaPlugin {

  private var shopUtil: ShopUtil = _

  def getShopUtil: ShopUtil = shopUtil

  private var levelUtil: LevelUtil = _

  def getLevelUtil: LevelUtil = levelUtil

  private var cs: CSUtility = _

  def getCSUtility: CSUtility = cs

  private var database: Database = _

  def getDatabase: Database = database

  private var upgradeUtil: UpgradeUtil = _

  def getUpgradeUtil: UpgradeUtil = upgradeUtil

  private var tagUtil: TagUtil = _

  def getTagUtil: TagUtil = tagUtil

  private var rds: RDSAdapter = _

  def getRDSAdapter: RDSAdapter = rds

  private var nick: Nick = _

  def getNick: Nick = nick

  override def onEnable(): Unit = {
    Wars.instance = this
    // コンフィグがあると期待する
    saveDefaultConfig()
    WarsAPI.reloadItem()
    if(!setupEconomy) {
      getLogger.severe("Vault or Economy plugin isn't loaded!! Wars was disabled")
      getServer.getPluginManager.disablePlugin(this)
      return
    }

    cs = new CSUtility()
    shopUtil = new ShopUtil(this, getConfig.getConfigurationSection("shops"))
    levelUtil = new LevelUtil(this)
    upgradeUtil = new UpgradeUtil(this, getConfig.getConfigurationSection("upgrades"))
    tagUtil = new TagUtil(this, getConfig.getConfigurationSection("tags"))
    database = new Database(this)
    rds = new RDSAdapter(this)

    Bukkit.getPluginManager.registerEvents(new PlayerListener(this), this)
    getCommand("item").setExecutor(new ItemCommand(this))
    //getCommand("wartp").setExecutor(new WarsCommand)
    getCommand("shop").setExecutor(new ShopCommand(this))
    getCommand("upgrade").setExecutor(new UpgradeCommand(this))
    getCommand("level").setExecutor(new LevelCommand(this))
    getCommand("tag").setExecutor(new TagCommand(this))

    nick = Bukkit.getPluginManager.getPlugin("Nick").asInstanceOf[Nick]
  }

  import collection.JavaConverters._

  override def onDisable(): Unit = {
    //WorldLoader.syncUnloadWorld("tdm")
    getDatabase.updateWPlayer(new util.ArrayList[Player](Bukkit.getOnlinePlayers).asScala.toSeq:_*)
    rds.hikari.close()
  }

  private def setupEconomy: Boolean = {
    if(getServer.getPluginManager.getPlugin("Vault") == null) {
      return false
    }
    val rsp = getServer.getServicesManager.getRegistration(classOf[Economy])
    if(rsp == null) return false
    Wars.econ = rsp.getProvider
    Wars.econ != null
  }
}

object Wars {
  protected[v2] var instance: Wars = _
  def getInstance: Wars = instance

  protected[v2] var econ: Economy = _
  def getEcon: Economy = econ
}
