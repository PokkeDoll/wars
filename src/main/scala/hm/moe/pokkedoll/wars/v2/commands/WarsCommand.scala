package hm.moe.pokkedoll.wars.v2.commands

import org.bukkit.Bukkit
import org.bukkit.command.{Command, CommandExecutor, CommandSender, ConsoleCommandSender}

class WarsCommand extends CommandExecutor {
  override def onCommand(sender: CommandSender, command: Command, label: String, args: Array[String]): Boolean = {
    sender match {
      case _: ConsoleCommandSender =>
        if(args.length != 0) {
          val player = Bukkit.getPlayer(args(0))
          if(player != null) {
            val world = Bukkit.getWorld("war")
            player.teleport(world.getSpawnLocation)
          }
        }
        true
      case _ =>
        false
    }
  }
}
