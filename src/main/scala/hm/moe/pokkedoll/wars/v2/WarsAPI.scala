package hm.moe.pokkedoll.wars.v2

import java.io.File

import hm.moe.pokkedoll.wars.v2.save.WPlayer
import org.bukkit.block.Block
import org.bukkit.configuration.ConfigurationSection
import org.bukkit.entity.{EntityType, Firework, Player}
import org.bukkit.inventory.{Inventory, ItemStack}
import org.bukkit._

import scala.collection.mutable
import scala.util.{Random, Try}

object WarsAPI {
  private lazy val plugin = Wars.getInstance

  var mapInfo: WMapLocation = _

  lazy val lobby: World = Bukkit.getWorlds.get(0)

  val items = mutable.HashMap.empty[String, ItemStack]

  val wplayers = mutable.HashMap.empty[Player, WPlayer]

  private val rand = new Random()

  val invalid = new ItemStack(Material.STONE, 1)

  def reloadItem(): Unit = {
    items.clear()
    plugin.reloadConfig()
    if (plugin.getConfig.isConfigurationSection("items")) {
      plugin.getConfig.getConfigurationSection("items").getKeys(false).forEach(key => {
        items.put(key, plugin.getConfig.getItemStack(s"items.$key", invalid))
      })
    }
  }

  def saveItem(): Unit = {
    plugin.saveConfig()
  }

  def setItem(key: String, item: ItemStack): Unit = {
    items.put(key, item)
    plugin.getConfig.set(s"items.$key", item)
  }

  def removeItem(key: String): Unit = {
    items.remove(key)
    plugin.getConfig.set(s"items.$key", null)
  }

  def getStats(name: String): String = "テスト"

  def getStats(wp: WPlayer): String = {
    s"§7========== §a${wp.player.getName}'s Stats §7==========\n" +
      s"§9Level: ${wp.level}\n" +
      s"§9Exp: ${wp.exp}\n" +
      "\n"
  }

  private val killRanking = mutable.HashMap.empty[String, (Int, Int)]
  private val deathRanking = mutable.HashMap.empty[String, (Int, Int)]
  private val assistRanking = mutable.HashMap.empty[String, (Int, Int)]

  val SETTING_INVENTORY_TITLE = "§bSettings"
  val TAG_INVENTORY_TITLE = "§c§lTAG"

  def getSettingInventory(wp: WPlayer): Inventory = {
    val inv = Bukkit.createInventory(null, 1, SETTING_INVENTORY_TITLE)
    /*
    val dummy: ItemStack = new ItemStack(Material.STAINED_GLASS_PANE, 1, 8) {
      val meta: ItemMeta = getItemMeta
      meta.setDisplayName(" ")
      setItemMeta(meta)
    }
    val tag_show: ItemStack = new ItemStack(Material.INK_SACK, 1) {
      val meta: ItemMeta = getItemMeta
      if (wp.show) {
        meta.setDisplayName("§aタグを表示する: ON")
        setDurability(10)
      } else {
        meta.setDisplayName("§7タグを表示する: OFF")
        setDurability(8)
      }
      meta.setLore(util.Arrays.asList("§fタグの表示設定"))
      setItemMeta(meta)
    }
    val tag_set: ItemStack = new ItemStack(Material.NAME_TAG, 1) {
      val meta: ItemMeta = getItemMeta
      if (wp.tag.equalsIgnoreCase("")) {
        meta.setDisplayName("§f現在のタグ: §7null")
      } else {
        meta.setDisplayName("§f現在のタグ: §7defined")
      }
      meta.setLore(util.Arrays.asList("§7表示するタグの設定"))
    }
    val stats: ItemStack = new ItemStack(Material.IRON_SWORD, 1) {
      val meta: ItemMeta = getItemMeta
      meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES)
      meta.setDisplayName("§9統計")
      meta.setLore(util.Arrays.asList(""))
      setItemMeta(meta)
    }
    val effect_kill: ItemStack = new ItemStack(Material.GLOWSTONE_DUST, 1) {
      val meta: ItemMeta = getItemMeta
      if (wp.killEffect.equals("")) {
        meta.setDisplayName("§f現在のキルエフェクト: §7null")
      } else {
        meta.setDisplayName("§f現在のキルエフェクト: defined")
      }
      meta.setLore(util.Arrays.asList("§fキルエフェクトを変更"))
    }
    val effect_sound: ItemStack = new ItemStack(Material.JUKEBOX, 1) {
      val meta: ItemMeta = getItemMeta
      if (wp.killSound.equals("")) {
        meta.setDisplayName("§f現在のキルサウンド: §7null")
      } else {
        meta.setDisplayName("§f現在のキルサウンド: defined")
      }
      meta.setLore(util.Arrays.asList("§fキルサウンドを設定"))
    }
    inv.setItem(0, dummy)
    inv.setItem(1, dummy)
    inv.setItem(2, tag_show)
    inv.setItem(3, tag_set)
    inv.setItem(4, stats)
    inv.setItem(5, effect_kill)
    inv.setItem(6, effect_sound)
    inv.setItem(7, dummy)
    inv.setItem(8, dummy)
     */
    inv
  }

  private def getWMapLocation(cs: ConfigurationSection): Option[WMapLocation] = {
    val red = LocationSerializer.string2location(cs.getString("red")).getOrElse(return None)
    val blue = LocationSerializer.string2location(cs.getString("blue")).getOrElse(return None)
    Some(new WMapLocation(red, blue))
  }

  def getWPlayer(player: Player): WPlayer = wplayers.getOrElseUpdate(player, plugin.getDatabase.loadWPlayer(player))

  def rmWPlayer(player: Player): Option[WPlayer] = wplayers.remove(player)

  private val csm = Bukkit.getScoreboardManager.getMainScoreboard

  def getItemName(item: ItemStack): String =
    if(item.hasItemMeta) if(item.getItemMeta.hasDisplayName) item.getItemMeta.getDisplayName else "" else ""

  def randomChance(chance: Double): Boolean = (chance / 100.0) > Math.random()

  def spawnFirework(location: Location): Unit = {
    val firework: Firework = location.getWorld.spawnEntity(location, EntityType.FIREWORK).asInstanceOf[Firework]
    val meta = firework.getFireworkMeta
    val effect: FireworkEffect = FireworkEffect.builder().`with`(FireworkEffect.Type.BALL).withColor(Color.YELLOW).build()
    meta.addEffect(effect)
    firework.setFireworkMeta(meta)
  }

  def getItemKey(item: ItemStack): String = {
    items.find(p => p._2.isSimilar(item)) match {
      case Some(v) => v._1
      case _ => ""
    }
  }

  def changeSkin(player: Player, skin: String): Unit = {
    plugin.getNick.getSkin.changeSkin(player, skin, false)
  }
}
