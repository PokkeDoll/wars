package hm.moe.pokkedoll.wars.v2.save

import org.bukkit.ChatColor
import org.bukkit.entity.Player

final class WPlayer(val player: Player) extends WFun with WStats {
  def sendMessage(message: String): Unit = player.sendMessage(ChatColor.translateAlternateColorCodes('&', message))
  override var tag: String = _
  override var tagContainer: Vector[String] = _
  override var sound: String = _
  override var soundContainer: Vector[String] = _
  override var effect: String = _
  override var effectContainer: Vector[String] = _

  override var exp: Int = _
  override var level: Int = _
}
