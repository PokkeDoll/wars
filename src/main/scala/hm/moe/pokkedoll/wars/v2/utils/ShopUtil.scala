package hm.moe.pokkedoll.wars.v2.utils

import java.util

import hm.moe.pokkedoll.wars.v2.{Wars, WarsAPI}
import hm.moe.pokkedoll.wars.v2.save.WPlayer
import hm.moe.pokkedoll.wars.v2.utils.ShopUtil.ShopItem
import org.bukkit.{Bukkit, Material}
import org.bukkit.configuration.ConfigurationSection
import org.bukkit.inventory.{Inventory, ItemFlag, ItemStack}

import scala.collection.mutable
import scala.jdk.CollectionConverters._

class ShopUtil(plugin: Wars, cs: ConfigurationSection) {

  private val config = plugin.getConfig

  val shops = mutable.HashMap.empty[String, Vector[ShopItem]]

  def reload(): Unit = {
    shops.clear()
    cs.getKeys(false).forEach(f => shops.put(f, cs.getConfigurationSection(f).getKeys(false).asScala.toVector.map(ff => new ShopItem(ff, cs.getInt(s"$f.$ff.price", 0), cs.getInt(s"$f.$ff.level", 0))).sortBy(_.level)))
  }

  def createInventory(wp: WPlayer, key: String): Option[Inventory] = {
    val s = shops.getOrElse(key, return None)
    val size = if( 9 >= s.length) 27 else if (18 >= s.length) 36 else if (27 >= s.length) 45 else 54
    val inv = Bukkit.createInventory(null, size, key)
    var c = 9
    if(size==27) {
      inv.setItem(0, upperShopInventory)
    } else if(size==36) {
      inv.setItem(0, upperShopInventory2)
    } else if(size==45) {
      inv.setItem(0, upperShopInventory3)
    } else {
      inv.setItem(0, upperShopInventory4)
    }
    inv.setItem(size-9, lowerShopInventory)
    s.foreach(f => {
      // アイテム情報設定
      val i = f.item.clone()
      val meta = i.getItemMeta
      val adl = util.Arrays.asList("", s"§e価格: $$${f.price}", s"§9必要ランク: ${f.level}")
      if(meta == null) {
        println("wait wait meta is null!" + i.getType.toString)
      } else if(meta.hasLore) {
        val l = meta.getLore
        l.addAll(adl)
        meta.setLore(l)
      } else {
        meta.setLore(adl)
      }
      i.setItemMeta(meta)
      inv.setItem(c, i)
      c += 1
    })

    Some(inv)
  }

  @Deprecated
  def toStringList(vector: Vector[ShopItem]): Array[String] = (for(i <- vector) yield {i.toString}).toArray

  /**
   * ショップを作る
   */
  def createShop(shop: String): Unit = {
    config.set(s"shops.$shop.init.price", 0)
    config.set(s"shops.$shop.init.level", 0)
    plugin.saveConfig()
    reload()
  }

  /**
   * ショップを削除する
   */
  def deleteShop(shop: String): Unit = {
    config.set(s"shops.$shop", null)
    plugin.saveConfig()
    reload()
  }

  /**
   * ショップに新たに商品を追加する
   */
  def addShopItem(shop: String, id: String, price: Int, level: Int): Unit = {
    val path = s"shops.$shop.$id"
    config.set(s"$path.price", price)
    config.set(s"$path.level", level)
    plugin.saveConfig()
    reload()
  }

  def removeShopItem(shop: String, id: String): Unit = {
    config.set(s"shops.$shop.$id", null)
    plugin.saveConfig()
    reload()
  }

  reload()

  val lowerShopInventory: ItemStack = {
    val i = new ItemStack(Material.STONE_SPADE, 1, 1)
    val m = i.getItemMeta
    m.setDisplayName(" ")
    m.setUnbreakable(true)
    m.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE)
    i.setItemMeta(m)
    i
  }

  val upperShopInventory: ItemStack = {
    val i = new ItemStack(Material.STONE_SPADE, 1, 2)
    val m = i.getItemMeta
    m.setDisplayName(" ")
    m.setUnbreakable(true)
    m.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE)
    i.setItemMeta(m)
    i
  }

  val upperShopInventory2: ItemStack = {
    val i = new ItemStack(Material.STONE_SPADE, 1, 3)
    val m = i.getItemMeta
    m.setDisplayName(" ")
    m.setUnbreakable(true)
    m.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE)
    i.setItemMeta(m)
    i
  }

  val upperShopInventory3: ItemStack = {
    val i = new ItemStack(Material.STONE_SPADE, 1, 4)
    val m = i.getItemMeta
    m.setDisplayName(" ")
    m.setUnbreakable(true)
    m.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE)
    i.setItemMeta(m)
    i
  }

  val upperShopInventory4: ItemStack = {
    val i = new ItemStack(Material.STONE_SPADE, 1, 5)
    val m = i.getItemMeta
    m.setDisplayName(" ")
    m.setUnbreakable(true)
    m.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE)
    i.setItemMeta(m)
    i
  }
}

object ShopUtil {
  class ShopItem(val id: String, val price: Int, val level: Int) {

    lazy val item: ItemStack = WarsAPI.items.getOrElse(id, new ItemStack(Material.STONE, 1))

    override def toString: String = s"$id,$price,$level"
  }
}
