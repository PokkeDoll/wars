package hm.moe.pokkedoll.wars.v2.commands

import hm.moe.pokkedoll.wars.v2.{Wars, WarsAPI}
import net.md_5.bungee.api.chat.{ClickEvent, ComponentBuilder, HoverEvent, TextComponent}
import org.bukkit.ChatColor
import org.bukkit.command.{Command, CommandExecutor, CommandSender}
import org.bukkit.entity.Player

class TagCommand(plugin: Wars) extends CommandExecutor {

  val tagUtil = plugin.getTagUtil

  val config = plugin.getConfig

  override def onCommand(sender: CommandSender, command: Command, label: String, args: Array[String]): Boolean = {
    sender match {
      case player: Player =>
        val wp = WarsAPI.getWPlayer(player)
        if(args.length==0) {
          player.sendMessage("§c/tag <set [<タグ名>]|list>")
        } else if(args(0).equalsIgnoreCase("set")) {
          if(args.length > 1) {
            val tag = args(1)
            if(!tagUtil.tags.contains(tag)) {
              player.sendMessage("§cそのタグは存在しません")
            } else if(!wp.tagContainer.contains(tag)) {
              player.sendMessage("§cそのタグを所持していません")
            } else {
              tagUtil.setTag(wp, tag)
              player.sendMessage("§9タグを設定しました")
            }
          } else {
            tagUtil.setTag(wp, "")
            player.sendMessage("§9タグをリセットしました")
          }
        } else if (args(0).equalsIgnoreCase("list")) {
          val cb = new ComponentBuilder("所持しているタグ一覧: /tag set <tag>でタグを設定できます!\n")
          wp.tagContainer.foreach(f => {
            val tag = tagUtil.tags.getOrElse(f, "")
            cb.append(s"$f => $tag\n")
              .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, s"/tag set $f"))
              .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(tag).append("§fをセットします").create()))
          })
          player.spigot().sendMessage(cb.create():_*)
        } else if(args(0).equalsIgnoreCase("mod")) {
          if(sender.hasPermission("pokkedoll.4") && args.length>1) {
            if(args(1).equalsIgnoreCase("list")) {
              val sb = new StringBuilder("タグリスト\n")
              tagUtil.tags.foreach(f => {
                sb.append(s"${f._1} => ${f._2}\n")
              })
              player.sendMessage(sb.toString())
            } else if(args.length > 3 && args(1).equalsIgnoreCase("set")) {
              val id = args(2); val display = args(3)
              config.set(s"tags.$id", display)
              plugin.saveConfig()
              player.sendMessage(s"$id に $display をセットしました")
              tagUtil.reload()
            } else if(args.length > 2 && args(1).equalsIgnoreCase("remove")) {
              val id = args(2)
              config.set(s"tags.$id", null)
              plugin.saveConfig()
              player.sendMessage(s"$id を削除しました")
              tagUtil.reload()
            } else {
              player.sendMessage("§c/tag mod <list|set <識別名> <表示名>|remove <識別名>")
            }
          }
        }
      case _ =>
    }
    true
  }
}
